Changelog
=========

version 1.0.0
* rename namespace ua2 -> sdh
* remove python2 compatibility
* minimal redis version 3.5


version 0.0.3
* fix compatibility with python 3.x

version 0.0.1

* Inital verstion

